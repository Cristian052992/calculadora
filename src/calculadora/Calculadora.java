package calculadora;

import java.util.Scanner;

public class Calculadora {

    public static void main(String[] args) {
        Scanner reader = new Scanner(System.in);
        double n1 = 0;
        double n2 = 0;
        int operacion = 0;
        String salir = "1";
        while ("1".equals(salir)) {
            System.out.println("Favor Introducir el numero de la operacion que desea realizar: \n1)Suma \n2)Resta \n3)Multiplicacion \n4)Division");
            operacion = reader.nextInt();
            do {
                System.out.println("Ingresa el primer numero");
                n1 = reader.nextDouble();
                System.out.println("Ingresa el segundo numero");
                n2 = reader.nextDouble();
                System.out.println(Double.toString(n1));
                System.out.println(Double.toString(n2));
                if (Double.toString(n1).length() < 7 && Double.toString(n2).length() < 7) {
                    if (operacion == 1) {
                        Suma sum = new Suma(n1, n2);
                        sum.mostrarResultado();
                    } else {
                        if (operacion == 2) {
                            Resta res = new Resta(n1, n2);
                            res.mostrarResultado();
                        } else {
                            if (operacion == 3) {
                                Multiplicacion mul = new Multiplicacion(n1, n2);
                                mul.mostrarResultado();
                            } else {
                                if (operacion == 4) {
                                    Division div = new Division(n1, n2);
                                    div.mostrarResultado();
                                } else {
                                    System.out.println("La opcion ingresada es incorrecta.");
                                }
                            }
                        }
                    }
                } else {
                    System.out.println("Favor Introducir un numero menor de 7 digitos.");
                }
            } while (Double.toString(n1).length() > 7 && Double.toString(n2).length() > 7);
            System.out.println("Desea Continuar realizando operaciones. \n1)Si \n2)No");
            salir = reader.next().toString();
            System.out.println(salir);
        }
    }
}
