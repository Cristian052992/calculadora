/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import calculadora.Division;
import calculadora.Multiplicacion;
import calculadora.Resta;
import calculadora.Suma;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Usuario
 */
public class TestCalculadora {
    
    public TestCalculadora() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
     @Test
     public void sumaPositivos() 
     {
         System.out.println("S1-TC1: Sumando dos numeros positivos");
     
     Suma sum = new Suma(4,5);
         assertTrue(sum.getRes()==9);
}
     @Test
      public void sumaNegativos() 
     {
         System.out.println("S1-TC2: Sumar un numero negativo menor que el segundo introducido");
     
     Suma sum = new Suma(-2,1);
         assertTrue(sum.getRes()==-1);
}
      
     @Test
     public void restaNegativos() 
     {
         System.out.println("S1-TC3: Utilizar en n1 y n2 numeros negativos con la finalidad de obtener la diferencia de ambos.");
     
     Resta res = new Resta(-10,-1);
         assertTrue(res.getRes()==-9);
    }

     @Test
     public void multiplicacionporcero() 
     {
         System.out.println("S1-TC4: Utilizar 0 y un numero negativo para realizar la multiplicacion.");
     
         Multiplicacion mul = new Multiplicacion(-10,0);
         assertTrue(mul.getRes()==0);
    }
    
     @Test
     public void divisionporcero() 
     {
         System.out.println("S1-TC5: Dividir un numerador entre 0 para verificar el manejo de esta indeterminacion matematica");
     
         Division div = new Division(5,0);
         assertTrue(div.getestadodiv()=="no se puede division entre cero Indefinido");
    }
}
